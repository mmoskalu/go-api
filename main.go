package main

import (
	"fmt"
	"go-api/controllers"
	"go-api/database"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	// Load Configurations from config.json using Viper
	LoadAppConfig()

	// Initialize Database
	database.Connect(AppConfig.ConnectionString)
	database.Migrate()

	// Initialize the router
	router := mux.NewRouter().StrictSlash(true)

	// Register Routes
	RegisterProductRoutes(router)

	// Start the server
	log.Printf("Starting Server on port %s", AppConfig.Port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%v", AppConfig.Port), router))
}

func RegisterProductRoutes(router *mux.Router) {
	router.HandleFunc("/api/products", controllers.List).Methods("GET")
	router.HandleFunc("/api/products/{id}", controllers.GetById).Methods("GET")
	router.HandleFunc("/api/products", controllers.Create).Methods("POST")
	router.HandleFunc("/api/products/{id}", controllers.Update).Methods("PUT")
	router.HandleFunc("/api/products/{id}", controllers.Delete).Methods("DELETE")
}
